'use strict';

(function () {
    const signUpButton = $('#signUp'),
        signInButton = $('#signIn'),
        container = $('#container');

    signUpButton.click(function () {
        container.addClass('right-panel-active');
    });

    signInButton.click(function () {
        container.removeClass('right-panel-active');
    });
})();
